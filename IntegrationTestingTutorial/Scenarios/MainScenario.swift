//
//  MainScenario.swift
//  IntegrationTestingTutorial
//
//  Created by Ilya Bondarenko on 5/21/19.
//  Copyright © 2019 DNTL. All rights reserved.
//

import Foundation
import UIKit

class MainScenario: GeneralScenario {
    
    // The default window of application
    private(set) var window: UIWindow!
    
    // The home view controller. This view controller is root view controller for all application.
    private(set) var homeVC: HomeVC?
    
    // The login scenario for an auth logic
    private(set) var loginScenario: LoginScenario?
    
    // Init method
    
    init(window: UIWindow!) {
        super.init()
        self.window = window
    }
    
    // Scenario Protocol methods
    
    override func start() {
        homeVC = HomeVC()
        window.rootViewController = homeVC
        
        if !AuthManager.shared.isHasToken {
            loginScenario = LoginScenario(rootVC: homeVC!)
            loginScenario?.start()
        }
    }
    
    override func stop() {}
}
