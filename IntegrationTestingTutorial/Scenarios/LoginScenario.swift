//
//  LoginScenario.swift
//  IntegrationTestingTutorial
//
//  Created by Ilya Bondarenko on 5/21/19.
//  Copyright © 2019 DNTL. All rights reserved.
//

import Foundation
import UIKit

class LoginScenario: GeneralScenario {
    
    // The login view controller
    private(set) var loginVC: LoginVC?
    
    // Init method
    init(rootVC: UIViewController) {
        super.init()
        self.rootVC = rootVC
    }
    
    // Scenario protocol methods
    override func start() {
        loginVC = LoginVC()
        rootVC?.present(loginVC!, animated: false, completion: nil)
    }
    
    override func stop() {}
}
