//
//  Scenario.swift
//  IntegrationTestingTutorial
//
//  Created by Ilya Bondarenko on 5/21/19.
//  Copyright © 2019 DNTL. All rights reserved.
//

import Foundation
import UIKit

/// All scenarios in project should support this protocol
protocol Scenario {
    
    /// This method should do logic for start scenario
    func start()
    
    /// This method should do logic for stop scenario
    func stop()
}

class GeneralScenario: Scenario {
    
    // The root view controller for show another view controllers.
    // This property is optional.
    var rootVC: UIViewController?
    
    func start() {}
    
    func stop() {}
}
