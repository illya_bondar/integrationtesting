//
//  AuthManager.swift
//  IntegrationTestingTutorial
//
//  Created by Ilya Bondarenko on 5/21/19.
//  Copyright © 2019 DNTL. All rights reserved.
//

import UIKit

class AuthManager: NSObject {

    static let shared = AuthManager()
    
    private let tokenKey = "token"
    
    /// This method saving the new token value
    ///
    /// - Parameter token: the new token value
    func save(token: String?) {
        UserDefaults.standard.setValue(token, forKey: tokenKey)
    }
    
    // This property returns true if user has token. In another cases returns false
    var isHasToken: Bool {
        return (UserDefaults.standard.value(forKey: tokenKey) as? String) != nil
    }
}
