//
//  HomeVC.swift
//  IntegrationTestingTutorial
//
//  Created by Ilya Bondarenko on 5/21/19.
//  Copyright © 2019 DNTL. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {

    // Init method
    init() {
        super.init(nibName: "HomeVC", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // Override methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
